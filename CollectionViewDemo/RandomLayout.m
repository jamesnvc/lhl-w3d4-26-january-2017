//
//  RandomLayout.m
//  CollectionViewDemo
//
//  Created by James Cash on 26-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "RandomLayout.h"

@interface RandomLayout ()
@property (strong, nonatomic) NSArray* cellPoints;
@end

@implementation RandomLayout

- (void)prepareLayout
{
    // Calculating the bounds of the view on screen so we can make sure our cells are visible
    CGRect frame = self.collectionView.frame;
    CGFloat maxX = CGRectGetMaxX(frame);
    CGFloat maxY = CGRectGetMaxY(frame);


    NSMutableArray *sections = [[NSMutableArray alloc] init];
    NSInteger nsections = self.collectionView.numberOfSections;
    // for each section...
    for (int s = 0; s < nsections; s++) {
        NSMutableArray *sectionPoints = [[NSMutableArray alloc] init];
        NSInteger nItems = [self.collectionView numberOfItemsInSection:s];
        // for each item in each section...
        for (int i = 0; i < nItems; i++) {
            // make a random point inside the frame of the collectionView
            CGPoint itemCenter = CGPointMake(arc4random_uniform(maxX),
                                             arc4random_uniform(maxY));
            [sectionPoints addObject:[NSValue valueWithCGPoint:itemCenter]];
        }
        [sections addObject:sectionPoints];
    }
    // sections array looks like:
    // [
    //  section 1
    //      [ s1item1, s1item2],
    //  section 2
    //      [ s2item1, s2item2]
    // ]
    self.cellPoints = [sections copy];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // If doing a flow layout:
    //[super layoutAttributesForItemAtIndexPath:indexPath];
    // note that we can't just `alloc init` a LayoutAttributes object or we get an error
    UICollectionViewLayoutAttributes *attrs = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    // get the pre-computed point we made in prepareLayout
    NSValue *pointValue = self.cellPoints[indexPath.section][indexPath.item];
    attrs.center = [pointValue CGPointValue];

    // set a size
    attrs.size = CGSizeMake(100, 100);

    return attrs;
}

// this is the method that will actually be called by the collectionView
- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *attrs = [[NSMutableArray alloc] init];
    NSInteger nsections = self.collectionView.numberOfSections;
    for (int s = 0; s < nsections; s++) {
        NSInteger nItems = [self.collectionView numberOfItemsInSection:s];
        for (int i = 0; i < nItems; i++) {
            // looping over each item of each section, use the method we wrote above to compute layout of each item
            [attrs addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:s]]];
        }
    }

    return attrs;
}

@end
