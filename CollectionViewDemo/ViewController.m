//
//  ViewController.m
//  CollectionViewDemo
//
//  Created by James Cash on 26-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "RandomLayout.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
// this will be our data source
@property (nonatomic, assign) NSInteger numberOfItems;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // need to set this to allow selecting multiple cells
    self.collectionView.allowsMultipleSelection = YES;
    // initialize our data source
    self.numberOfItems = 10;
    // this is commented out while using our custom layout
    //UICollectionViewFlowLayout *layout = self.collectionView.collectionViewLayout;
    //layout.itemSize = CGSizeMake(100, 100);
    // use our new custom layout -- comment this out to use default flow layout
    self.collectionView.collectionViewLayout = [[RandomLayout alloc] init];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addItemsToCollection:(id)sender {

    self.numberOfItems += 1;
    // Below is the tableView equivalent
    // [tableView beginUpdates];
    // [tableView insertITems ...];
    // [tableView endUpdates];

    // Note the caret: this is a "block" that will actually be called inside collectionView when it needs it
    [self.collectionView performBatchUpdates:^{
        // Note that this doesn't actually change the data, just notifies the collectionview that the data has changed
        // try commenting out the "+= 1" line above or changing the number to see the error that results
        [self.collectionView
         insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]]];
    } completion:^(BOOL finished) {
        NSLog(@"Finished adding things");
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.numberOfItems;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"demoCell" forIndexPath:indexPath];

    // we need to actually create the selectedBackgroundView (which will be displayed when the cell is selected
    cell.selectedBackgroundView = [[UIView alloc] init];
    cell.selectedBackgroundView.backgroundColor = [UIColor redColor];

    return cell;
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Should select %@?", indexPath);
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected %@", indexPath);
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"De-selected %@", indexPath);
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.item % 2 == 0;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Dynamically computing the  size of items
    // note this is a flowlayout delegate method, so it won't affect our custom layout
    return CGSizeMake(50 + indexPath.item * 5,
                      50 + indexPath.item *5);
}

@end
